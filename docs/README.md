The folder with data is absent from the git repo.
- Create folders "/data/1_raw_movies_and_xmls"
- put the tif stacks and corresponding xmls files there.
- run the notebooks that generate training images + labels.
