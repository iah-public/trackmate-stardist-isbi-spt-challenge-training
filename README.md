# Training of a StarDist model for the detection for sub-resolved particles.

This repository contains the scripts, model and parameters used to train a StarDist model on the dataset of the 2014 ISBI Grand Challenge on Single-Particle Tracking.
See http://bioimageanalysis.org/track/ for information on the challenge.

We used this model to compare the performance of a tracking implementation based on StarDist, versus its counterpart based on a classic Laplacian of Gaussian (LoG) detector.
Both approaches are subsequently used in TrackMate.

- Raw data: synthetic time-lapses of different particles (microtubules, vesicles) in different SNR and corresponding ground truth coordinates of the particles stored in xml.
- Step 1: generate training data: Image + labeled image. Image will be same as those in the raw dataset, and the labeled image will be generated out of coordinates stored in corresponding xml files.
- Step 2: use the pairs image+labeled image to train Stardist models
- Step 3: test the models.

Read more in ./docs/TransferNotes.md
